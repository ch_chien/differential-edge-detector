#ifndef CPU_CANNY_HPP
#define CPU_CANNY_HPP

class CannyCPU { 
    int img_height;
    int img_width;
    int kernel_sz;
    int gauss_sigma;
    double highThr;
    double lowThr;
    
    double **img;
    double **grad_mag;
	  double **Ix, **Iy;
    double **subpix_edge_pts_strong;
    double **subpix_edge_pts_weak;
    double **subpix_edge_pts_weak2strong;
    double **subpix_edge_pts_final;
    double **subpix_idx_map;

  public:

    int num_of_candidate_strong;
    int num_of_candidate_weak;

    int num_of_final_edge_pts;
    int edge_pt_sz;

    CannyCPU(int, int, int, int, double);
    ~CannyCPU();

    void preprocessing(std::ifstream& scan_infile);
    void convolve_img();
    void non_maximum_suppresion();
    void edge_hysteresis();

    void read_array_from_file(std::string filename, double **rd_data, int first_dim, int second_dim);
    void write_array_to_file(std::string filename, double **wr_data, int first_dim, int second_dim);
};



#endif