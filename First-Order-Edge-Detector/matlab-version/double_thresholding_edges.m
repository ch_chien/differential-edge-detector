function [doubleThrEdgeMap] = double_thresholding_edges(gen_edges, highThr, drawEdgesWithColors)
%function [doubleThrEdgeMap] = double_thresholding_edges(gen_edges, highThrRatio, lowThrRatio)
% 
% Parameters:
%   gen_edges:       generic edge map after doing NMS
%   highThrRatio:    high threshold ratio
%   lowThrRatio:     low threshold ratio
%   
% Output
%   doubleThrEdgeMap: edge map after doing double thresholding
%
%
% (c) LEMS, Brown University
% Chiang-Heng Chien (chiang-heng_chien@brown.edu)
% March 2022

%% Preprocess: define high and low thresholds
%highThr = max(gen_edges(:,4))*highThrRatio;
%lowThr = highThr * lowThrRatio;
lowThr = 0.4 * highThr;

%% define strong, medium, weak edges --
doubleThrEdgeMap = [gen_edges, zeros(size(gen_edges, 1), 1)];
for i = 1:size(gen_edges, 1)
    if gen_edges(i,4) >= highThr
        continue;
    elseif gen_edges(i,4) < highThr && gen_edges(i,4) > lowThr
        doubleThrEdgeMap(i,end) = 1;
    else
        doubleThrEdgeMap(i,end) = 2;
    end
end

% -- DEBUG ONLY --
genericED_debugging = 1;
if genericED_debugging
    strongMask = doubleThrEdgeMap(:,end) == 0;
    mediumMask = doubleThrEdgeMap(:,end) == 1;
    weakMask = doubleThrEdgeMap(:,end) == 2;
    numOfStrongEdges = sum(strongMask);
    numOfMediumEdges = sum(mediumMask);
    numOfWeakEdges = sum(weakMask);
    fprintf('number of strong, medium, weak edges: %d, %d, %d\n', numOfStrongEdges, numOfMediumEdges, numOfWeakEdges);
end

% -- for showing three kinds of edges --
if drawEdgesWithColors
    figure;
    for i = 1:size(doubleThrEdgeMap, 1)
        if doubleThrEdgeMap(i,end) == 0
            color = 'b.';
        elseif doubleThrEdgeMap(i,end) == 1
            color = 'r.';
        else
            color = 'g.';
        end
        plot(doubleThrEdgeMap(i,1), doubleThrEdgeMap(i,2), color);
        hold on;
    end
    axis equal;
    set(gcf,'color','w');
end

%% delete weak edges
idx = find(doubleThrEdgeMap(:,end)==2);
doubleThrEdgeMap(idx,:) = [];