function hyst_edges = edges_hysteresis(doubleThrEdgeMap, img)
%function [doubleThrEdgeMap] = edges_hysteresis(doubleThrEdgeMap, img)
% 
% Parameters:
%   doubleThrEdgeMap:       edge map after doing double-thresholding
%   img:                    input raw image array
%   
% Output
%   hyst_edges:             edge map after doing edge hysteresis
%
%
% (c) LEMS, Brown University
% Chiang-Heng Chien (chiang-heng_chien@brown.edu)
% March 2022

%% Preprocess: map double edge thresholding edges to a canvas
[h, w, ~] = size(img);
% edge_size = size(doubleThrEdgeMap, 1);
canvas = 100*ones(h, w, 2);
canvas(:,:,2) = 0;
for i = 1:size(doubleThrEdgeMap, 1)
    pix_x = doubleThrEdgeMap(i,5);
    pix_y = doubleThrEdgeMap(i,6);
    
    % -- boolean indicating strong or medium edge point --
    % -- 0: strong; 1: weak --
    canvas(pix_y, pix_x, 1) = doubleThrEdgeMap(i,7);
    canvas(pix_y, pix_x, 2) = i;
end
hysteresis_canvas = canvas;

% DEBUG ONLY
edge_map1 = canvas(:,:,1);
idx_map1 = canvas(:,:,2);

%% Decide whether weak edge points should be kept as an edge point 
window_sz = 3;
exceed_margin = floor(window_sz/2);
neighbors = zeros(window_sz*window_sz-1, 4);

for r = 1+exceed_margin : h-exceed_margin
    for c = 1+exceed_margin : w-exceed_margin
        % -- for strong edge points --
        if canvas(r, c, 1) == 0
            % -- recursively find and convert weak points locating at the
            % neighbors of strong points --
            update_canvas = recursive_hysteresis(canvas, r, c, exceed_margin, neighbors);
            canvas = update_canvas;
        end
    end
end

% DEBUG ONLY
edge_map2 = canvas(:,:,1);
idx_map2 = canvas(:,:,2);

%% map the hysteresis_canvas back to a list of edge points
edge_count = 1;
for r = 1 : h
    for c = 1 : w
        if canvas(r,c,1) == 0
            edge_idx(edge_count,1) = canvas(r,c,2);
            edge_count = edge_count + 1;
        end
    end
end

hyst_edges = doubleThrEdgeMap(edge_idx(:,1),:);


