function update_canvas = recursive_hysteresis(canvas, r, c, check_margin, neighbors)
%function update_canvas = recursive_hysteresis(canvas, r, c, check_margin, neighbors)
% 
% Parameters:
%   canvas:         image canvas mapped from the double thresholding edge map
%   r:              input row index of the canvas
%   c:              input column index of the canvas
%   check_margin:   the neighboring area of the strong point ar (r, c)
%   neighbors:      a zero array storing information of neighbors around strong point at (r, c)
%   
% Output
%   update_canvas:  updated canvas after finding and converting weak edge
%                   points that locate in the neighbor of strong points ar (r, c)
%
%
% (c) LEMS, Brown University
% Chiang-Heng Chien (chiang-heng_chien@brown.edu)
% March 2022

nidx = 1;
[h,w,~] = size(canvas);

% -- first store information (row, column, strong/weak indicator, etc) in a
% neighbor array --
for i = -check_margin:check_margin
    for j = -check_margin:check_margin
        if (r+i > h) || (c+j > w)
            neighbors(nidx, 1) = 100;
            nidx = nidx + 1;
            continue;
        else
            neighbors(nidx, 1) = canvas(r+i,c+j,1);
            neighbors(nidx, 2) = r+i;
            neighbors(nidx, 3) = c+j;
            nidx = nidx + 1;
        end
    end
end

% -- check whether the strong point's neighbor has any weak
% points or not. If there is one, make that weak point as a 
% strong point --
[weak_idx, ~] = find(neighbors(:,1)==1);
if ~isempty(weak_idx)
    for pts = 1:size(weak_idx, 2)
        convert_idx = weak_idx(pts);
        canvas(neighbors(convert_idx, 2), neighbors(convert_idx, 3), 1) = 0;

        % -- if there is any weak point converted to a strong point, target 
        % that converted point and check its neighbors until no single weak
        % point is around a strong point. This can be done recursively. --
        canvas = recursive_hysteresis(canvas, neighbors(convert_idx, 2), neighbors(convert_idx, 3), check_margin, neighbors);
    end
    update_canvas = canvas;
else
    % -- if no weak points around a strong point, exit --
    update_canvas = canvas;
end
       
end