% First-Order Edge Detector (Canny Edge Detector)
% (c) LEMS, Brown University
% Chiang-Heng Chien (chiang-heng_chien@brown.edu)
% March 2022
% Part of the code are from https://github.com/yuliangguo/Differential_Geometry_in_Edge_Detection

clear all; close all;

genericED_debugging = 0;

input_img_name = '2018';
dst_path = '/home/chchien/BrownU/differential-edge-detector/First-Order-Edge-Detector/matlab-version/';
%str_readPath = strcat(dst_path, input_img_name, '.jpg');
str_readPath = strcat(dst_path, input_img_name, '.pgm');
img = imread(str_readPath);
opts.w = size(img,2)-1;
opts.h = size(img,1);

img = img(:,1:opts.w);

%% 1. First-Order Edge Detection: Convolve Gaussian Derivative Filter and NMS (STEP 1~4)
drawEdgeMap_afterNMS = 0;

thresh = 2;
sigma = 2;
n = 1;
[gen_edges, ~] = first_order_edge_detector(img, sigma, n, thresh);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% outputs:
% gen_edges = [Subpixel_X Subpixel_Y Orientation(theta) Confidence, Pix_X, Pix_Y]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if drawEdgeMap_afterNMS
    fprintf('drawing generic edge map after NMS (non-maximal suppression):');
    disp_edg(gen_edges, opts);
end
fprintf('\n');

%% 2. Refine Edges by Double Thresholding and Edge Hysteresis (STEP 5 and 6)
drawEdgeMap_afterDoubleThr = 1;

% -- double thresholding --
highThr = 10;
[doubleThrEdgeMap] = double_thresholding_edges(gen_edges, highThr, drawEdgeMap_afterDoubleThr);

%% 3. Edge Hysteresis (Edge Tracking)
% -- Edge Hysteresis --
hyst_edges = edges_hysteresis(doubleThrEdgeMap, img);

% -- draw the resultant edge map --
drawEdgeMap_afterHysteresis = 0;
if drawEdgeMap_afterHysteresis
    fprintf('drawing generic edge map after edge hysteresis');
    figure;
    for i = 1:size(hyst_edges, 1)
        plot(hyst_edges(i,1), hyst_edges(i,2), 'b.');
        hold on;
    end
    axis equal;
    set(gcf,'color','w');
end
fprintf("\n");
