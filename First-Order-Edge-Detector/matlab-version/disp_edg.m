function disp_edg(edg, opts, col)
%function disp_edg(edg, col)
%
% Display the edgemap on the current figure in the given color
%   The edgels are displayed as little line segments.
%
% (c) LEMS, Brown University
% Amir Tamrakar (amir_tamrakar@brown.edu)
% October 2007
% Revised by Chiang-Heng Chien (chiang-heng_chien@brown.edu)
% March 2022

% Using little line segment on an edge point to represent a small edge:
%               + (edge(i,1)+0.5*cos(edge(i,3)), edge(i,2)+0.5*sin(edge(i,3)))
%              /
%             + (edge(i,1), edge(i,2))
%            /
%           + (edge(i,1)-0.5*cos(edge(i,3)), edge(i,2)-0.5*sin(edge(i,3)))

figure;
hold on;


[m,n] = size(edg);

% change edg to matlab coordinates
edg(:,1) = edg(:,1) +1;
edg(:,2) = edg(:,2) +1;

% plot the edgels
for i=1:m
    if(nargin < 3)
        plot([edg(i,1)+0.5*cos(edg(i,3)) edg(i,1)-0.5*cos(edg(i,3))], [edg(i,2)+0.5*sin(edg(i,3)) edg(i,2)-0.5*sin(edg(i,3))], 'b');
    else
        plot([edg(i,1)+0.5*cos(edg(i,3)) edg(i,1)-0.5*cos(edg(i,3))], [edg(i,2)+0.5*sin(edg(i,3)) edg(i,2)-0.5*sin(edg(i,3))], 'color', col);        
    end
        %plot(edg(i,1), edg(i,2), 'r.');
end

%hold off;

axis equal;
set(gcf,'color','w');
