function [filt_sig] = filter_2d(sig, filt_fun_2d, sigma, n)
% function [filt_sig, dx] = filter_2d(sig, filt_fun_2d, sigma, n)
%
% Filter the signal at pixel locations using first-order Gaussian
% derivative filter
%
% Parameters:
%    sig:          original image to be filtered
%    filt_fun_2d:  a 2d filter kernel that can be resampled
%    n:            2^n number of subdivions within the pixel (NOT USED IN THE CURRENT VERSION) 
%
% Output:
%   filt_sig : resulting response signal 
%
% (c) LEMS, Brown University
% Chiang-Heng Chien (chiang-heng_chien@brown.edu)
% March 2022

% compute the filter at this shift 
filt_resamp = filt_fun_2d(sigma, 0, 0);

% filter sig with this filter
%filt_sig = imfilter(sig, filt_resamp, 'conv','replicate');

%filt_sig = imfilter(sig, filt_resamp, 'conv');

%val_img = sig(9:489, 9:329);
val_img = sig;

filt_sig = zeros(size(val_img, 1), size(val_img, 2));
%Iy = zeros(size(val_img, 1), size(val_img, 2));
cent = 9;
for i = 1:size(val_img, 1)
    for j = 1:size(val_img, 2)
        
        for p = -8:8
            for q = -8:8
                if ((i+p) < 1 || (j+q) < 1 || (i+p) > size(val_img, 1) || (j+q) > size(val_img, 2))
                    continue;
                end
                
                filt_sig(i,j) = filt_sig(i,j) + val_img(i+p, j+q) * filt_resamp(p+cent, q+cent);
                %Iy(i,j) = Iy(i,j) + val_img(i+p, j+q) * filt_resamp(p+cent, q+cent);
            end
        end        
    end
end
	


% % -- for DEBUGGING --
% fileFolder = '/home/chchien/BrownU/research/Differential-Geometry-Edge-Detection/MyBitBucket/differential-edge-detector/First-Order-Edge-Detector/matlab-version/';
% fullOutputFileName = fullfile(fileFolder, "filter.txt");
% filterWr = fopen(fullOutputFileName, 'w');
% 
% for i = 1:size(filt_resamp, 1)
%     for j = 1:size(filt_resamp, 2)
%         fprintf(filterWr, string(filt_resamp(i,j)));
%         fprintf(filterWr, '\n');
%     end
%     %fprintf(filterWr, '\n');
% end
% 
% 
% fclose(filterWr);