function [gen_edge_map, grad_mag] = first_order_edge_detector(img, sigma, n, threshold)
%function [gen_edge_map, grad_mag] = first_order_edge_detector(img, sigma, n, threshold,QaD_flag)
% 
% --------------------------------
% First-order edge detector (v1.0)
% --------------------------------
%
% Summary:
%   Compute the gradient image and perform NMS to locate edges.
%
% Parameters:
%   img:       input image
%   sigma:     scale of the operator (>0.7), typically 1
%   n:         interpolating factor (2^n number of subdivions within the
%              pixel) typically 1. This is NOT USED for the first-order
%              edge detector.
%   threshold: gradient magnitude threshold (not normalized, typically 2)
%
% Output
%   gen_edge_map: Generic Edge map
%
% Note: An edgemap is a matrix of detected edgels 
%       (each row contains [subpixel_x, subpixel_y, theta, strength, pixel_x, pixel_y]
%
% See also filt_2d
%
% (c) LEMS, Brown University
% Chiang-Heng Chien (chiang-heng_chien@brown.edu)
% March 2022

%% Preprocess
if (size(img,3)>1) %convert from rgb to gray
    img = rgb2gray(img);
end
img = double(img);
margin = ceil(4*sigma); % -- margin=8 in current case --

%% pad the image
%img = padarray(img,[margin margin],'symmetric','both');
%img = padarray(img,[margin margin],0,'both');

%% Compute derivatives using Shifted Gaussian filters
Ix   = filter_2d(img, @Gx_2d_op, sigma, n);
Iy   = filter_2d(img, @Gy_2d_op, sigma, n);

%% Locate edges in the supixel scope
grad_mag = sqrt(Ix.^2+Iy.^2); %gradient magnitude

% We'll use NMS to detect the edgels - this NMS routine ouputs subpixel edgel tokens
[subpix_x, subpix_y, subpix_dir_x, subpix_dir_y, subpix_grad_x, subpix_grad_y, pix_x, pix_y] = NMS_token(Ix, Iy, grad_mag, grad_mag>threshold, margin);

mag_e = sqrt(subpix_grad_x.^2+subpix_grad_y.^2);

amp = 1;
% construct the edge map
%gen_edge_map = [(subpix_x'-1)/amp (subpix_y'-1)/amp atan2(subpix_dir_y',  subpix_dir_x')  mag_e' round((pix_x+1)'/amp) round((pix_y+1)'/amp)];
gen_edge_map = [subpix_x' subpix_y' atan2(subpix_dir_y',  subpix_dir_x')  mag_e' pix_x' pix_y'];
