# First-Order Edge Detection (Canny Edge Detection) in MATLAB

Part of the Code comes from https://github.com/yuliangguo/Differential_Geometry_in_Edge_Detection

## 1. Run the code

(1) see demo.m

(2) One sample image is already provided.

## 2. Basic Flow of the First-Order Edge Detector

STEP 1: Pad the image

STEP 2: Compute first-order Gaussian filter

STEP 3: Convolve the padded image with the filter computed from STEP 2

STEP 4: Do non-maximum suppression (NMS) to find initial edge points

STEP 5: Do double thresholding to decide strong, weak, and in-between edge points

STEP 6: Connect edges via edge hysteresis
